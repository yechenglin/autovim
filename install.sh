#!/bin/bash

# Copyright (c) 2019 Chenglin.Ye <yechnlin@gmail.com>

set -e

sudo apt-get install -y ctags cscope

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

install -m 664 ./vimrc ~/.vim/
sudo install -m 777 ./ct /usr/local/bin/

echo -e '\033[32mINFO:\033[0m'
echo -e '\033[32m  Install successfully...\033[0m'
