set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" default plugins
Plugin 'tpope/vim-fugitive'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" new plugins
Plugin 'https://github.com/vim-scripts/taglist.vim.git'
Plugin 'https://github.com/vim-scripts/cscope.vim.git'
Plugin 'https://github.com/fatih/vim-go.git'
Plugin 'https://github.com/scrooloose/nerdtree.git'
Plugin 'https://github.com/jlanzarotta/bufexplorer.git'
Plugin 'https://github.com/vim-scripts/SrcExpl.git'
Plugin 'https://github.com/vim-scripts/trailing-whitespace.git'
Plugin 'https://github.com/vim-scripts/DeleteTrailingWhitespace.git'
Plugin 'https://github.com/vim-scripts/vsearch.vim.git'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line



"""""""""""""""""""""""""""
" Common "
"""""""""""""""""""""""""""
set nu  "display the number of every line
set ruler   "show the cursor position all the time
"tab with two space
set tabstop=2
set shiftwidth=2
set expandtab
"display tab
"highlight SpecialKey ctermfg=1
"set list
"set listchars=tab:T-
"highlight search
set hlsearch
set noignorecase
"show pwd
noremap <C-p> :pwd<CR>
"highlight WhitespaceEOL ctermbg=red guibg=red
"match WhitespaceEOL /s\s\+$/
" highelight "

""""""""""""""""""""""""""""""
" TagList "
""""""""""""""""""""""""""""""
let Tlist_Show_One_File=1
let Tlist_Exit_OnlyWindow=1
"map <F2> :call Tlist()<CR>
map tl :call Tlist()<CR>
func! Tlist()
exec "TlistToggle"
endfun

"""""""""""""""""""""""""""
" NERDTree "
"""""""""""""""""""""""""""
noremap to :NERDTree<CR>
noremap tq :NERDTreeClose<CR>
noremap tp :NERDTreeFind<CR>
"noremap tt :NERDTreeToggle<CR>

"""""""""""""""""""""""""""
" BufExplorer "
"""""""""""""""""""""""""""
map bo :BufExplorer<CR>
map bt :ToggleBufExplorer<CR>
map bs :BufExplorerHorizontalSplit<CR>
map bv :BufExplorerVerticalSplit<CR>
"let g:bufExplorerSplitRight=1
"switch buffer
nmap bn :bn<CR>
nmap bp :bp<CR>

"""""""""""""""""""""""""""
" DeleteTrailingWhitespace "
"""""""""""""""""""""""""""
map cws :DeleteTrailingWhitespace<CR>

"""""""""""""""""""""""""""
" SrcExpl "
"""""""""""""""""""""""""""
map so :SrcExplToggle<CR>
let g:SrcExpl_winHeight = 13
let g:SrcExpl_refreshTime = 50
" // Set "Enter" key to jump into the exact definition context
let g:SrcExpl_jumpKey = "<ENTER>"
" // Set "Space" key for back from the definition context
let g:SrcExpl_gobackKey = "<SPACE>"
" // In order to avoid conflicts, the Source Explorer should know what plugins
" // except itself are using buffers. And you need add their buffer names into
" // below listaccording to the command ":buffers!"
let g:SrcExpl_pluginList = [
        \ "__Tag_List__",
        \ "_NERD_tree_"
    \ ]
" // Enable/Disable the local definition searching, and note that this is not
" // guaranteed to work, the Source Explorer doesn't check the syntax for now.
" // It only searches for a match with the keyword according to command 'gd'
let g:SrcExpl_searchLocalDef = 1
" // Do not let the Source Explorer update the tags file when opening
let g:SrcExpl_isUpdateTags = 0
" // Use 'Exuberant Ctags' with '--sort=foldcase -R .' or '-L cscope.files' to
" // create/update the tags file
let g:SrcExpl_updateTagsCmd = "ctags --sort=foldcase -R ."
" // Set "<F10>" key for updating the tags file artificially
let g:SrcExpl_updateTagsKey = "<F10>"
" // Set "<F5>" key for displaying the previous definition in the jump list
let g:SrcExpl_prevDefKey = "<F5>"
" // Set "<F4>" key for displaying the next definition in the jump list
let g:SrcExpl_nextDefKey = "<F4>"

"""""""""""""""""""""""""""
" Vsearch "
"""""""""""""""""""""""""""
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
