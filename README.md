autovim
=======

 Platform      | Build Status
 ------------- | ------------
 Linux(Ubuntu) |

Auto configure vim.

## How to build

* Install autovim

```
$ ./install.sh
```

* Install plugins

```
$ vim
```

then input `:PluginInstall` to install all plugins.
